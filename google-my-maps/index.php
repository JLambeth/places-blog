<?PHP

	$secretsFile = 'C:\\secure\\client_secret.json';
	$mapIdFile = 'C:\\secure\\map_id.txt';
	$tokenFile = 'token.json';
	$mapFile = 'map.kml';

	if(!file_exists($secretsFile))
		die('File containing client secrets does not exist - cannot continue.');

	// Open client secrets from location outside of web root, decode json to associative array
	$clientSecrets = file_get_contents($secretsFile);
	$clientSecrets = json_decode($clientSecrets, true);
	
	if(!empty($_GET['error'])){
		
		echo 'An error was returned during authentication: ' . htmlspecialchars($_GET['error'], ENT_QUOTES, 'UTF-8');
		
	}
	
	elseif(!empty($_GET['code'])){
		
		var_dump($_GET['code']);
		
		$postData = array(	'code' => $_GET['code'],
							'client_id' => $clientSecrets['web']['client_id'],
							'client_secret' => $clientSecrets['web']['client_secret'],
							'redirect_uri' => 'http://localhost/maps/index.php',
							'grant_type' => 'authorization_code');
							
		$postString = '';
		
		foreach($postData as $key => $value)			
			$postString .= $key . '=' . $value . '&';
			
		$postString = substr($postString, 0, -1);
							
		$ch = curl_init($clientSecrets['web']['token_uri']);
			
		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_CRLF, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);			
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		
		curl_setopt($ch, CURLOPT_CAINFO, 'C:/xampp/cacert.pem');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		var_dump($postString);
		
		curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);

		$response = curl_exec($ch);
		
		if($response !== false){
			
			$requestHeader = curl_getinfo($ch, CURLINFO_HEADER_OUT);
			$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
			list($responseHeader, $responseBody) = explode("\r\n\r\n", $response, 2);
			
			if($responseCode == 200){
							
				file_put_contents($tokenFile, $responseBody);
				header('Location: index.php?action=download');			
				
			}
			
		}
		
		if($response === false || $responseCode != 200) {
			
			if(!empty($responseHeader) && !empty($responseBody)){
				
				var_dump($requestHeader);
				var_dump($responseHeader);
				var_dump($responseBody);
				
			}
			
			die('Could not retrieve token from Google.');
			
		}
		
		curl_close($ch);	
		
	}
	
	elseif(!empty($_GET['action']) && $_GET['action'] == 'download'){
		
		if(!file_exists($tokenFile))
			die('Token file does not exist.');
		
		$fp = fopen($mapFile, 'w+');
		
		$token = json_decode(file_get_contents($tokenFile), true);
		$mid = file_get_contents($mapIdFile);
		
		$requestUrl = 'https://www.google.com/maps/d/kml?forcekml=1&mid=' . urlencode($mid);
		
		$ch = curl_init($requestUrl);
			
		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_CRLF, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);			
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		
		curl_setopt($ch, CURLOPT_CAINFO, 'C:/xampp/cacert.pem');
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: ' . $token['token_type'] . ' ' . $token['access_token']));
		
		curl_setopt($ch, CURLOPT_FILE, $fp);
		
		curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);

		$response = curl_exec($ch);
		
		if($response !== false){
			
			$requestHeader = curl_getinfo($ch, CURLINFO_HEADER_OUT);
			$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
		}
		
		if($response === false || $responseCode != 200) {
			
			die('Could not map token from Google.');
		
		}
		
		if(!empty($requestHeader) && !empty($responseCode)){
				
			var_dump($requestHeader);
			var_dump($responseCode);
			
		}
		
	}
	
	elseif(empty($_GET)){
	
		$oAuthParameters = array(	'response_type' => 'code',
									'client_id' => $clientSecrets['web']['client_id'],
									'redirect_uri' => 'http://localhost/maps/index.php',
									'scope' => 'https://www.googleapis.com/auth/mapsengine',
									'access_type' => 'offline');
		
		// Construct the authentication URI
		$authUri = $clientSecrets['web']['auth_uri'] . '?';
		
		// Add each of the parameters, url encoded
		foreach($oAuthParameters as $key => $value)			
			$authUri .= urlencode($key) . '=' . urlencode($value) . '&';
			
		// Trim the trailing ampersand
		$authUri = substr($authUri, 0, -1);
								
		// Redirect the user
		header('Location: ' . $authUri);
		
	}

?>